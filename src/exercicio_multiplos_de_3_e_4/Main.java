package exercicio_multiplos_de_3_e_4;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

public class Main {

	public static void main(String[] args) {
		List<Integer> listaInteiros = new ArrayList<Integer>();
		
		Integer quantidadeNumerosInseridos = 0;
		Integer numeroDigitado = 1;
		
		while (numeroDigitado != 0 && quantidadeNumerosInseridos < 1000){
			numeroDigitado = Integer.parseInt(JOptionPane.showInputDialog("Digite um n�mero entre 1 e 100 (ou 0 para sair)"));
			// se o usuario digitar 0, sai desta parte do programa e exibe o array invertido
			if (numeroDigitado == 0){
				break;
			}
			
			// caso o numero digitado seja menor que zero, pergunta outro numero pro usuario
			if (numeroDigitado < 0 || numeroDigitado > 100){
				JOptionPane.showMessageDialog(null,"Digite um n�mero entre 1 e 100!!!");
				continue;
			}
			
			// verifica se numero � divisivel por 3 e 4 ao mesmo tempo
			if ((numeroDigitado % 3 == 0) && (numeroDigitado % 4 == 0)){
				listaInteiros.add(numeroDigitado);
				quantidadeNumerosInseridos++;
			}
		}		
		
		// exibe o array de inteiros de tras para frente
		for (int i = (quantidadeNumerosInseridos - 1); i >= 0; i--){
			System.out.println(listaInteiros.get(i));
		}
	}
}
